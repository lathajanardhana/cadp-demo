﻿namespace my_tdd_application_api.Models
{
    public class User
    {
        public int userId { get; set; }

        public string userName { get; set; }

        public string description { get; set; }

        public Roles userRole { get; set; }

        public Course course { get; set; }
    }
}