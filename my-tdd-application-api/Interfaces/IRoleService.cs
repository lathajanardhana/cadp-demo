﻿using my_tdd_application_api.Models;
using System.Collections.Generic;

namespace my_tdd_application_api.Interfaces
{
    public interface IRoleService
    {
        public List<Roles> returnAllRoleList();

        public Roles returnRoleDetailsforRoleId(int roleId);

        public bool AddRoleToList(Roles myRole);

    }
}