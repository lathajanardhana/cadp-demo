﻿using my_tdd_application_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_tdd_application_api.Interfaces
{
    public interface ICourseService
    {
        public List<Course> returnAllCourses();

        public bool AddCourse(Course myCourse);
    }
}
