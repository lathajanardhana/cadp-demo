﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;



namespace contractTesting.Test
{
    class Program
    {
        protected Program() { }
        public static void Main(string[] args)
        {
           WebHost.CreateDefaultBuilder().UseStartup<TestStartup>().Build().Run();
       }
    }
}
