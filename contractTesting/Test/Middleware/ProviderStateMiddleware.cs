using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using my_tdd_application_api.Models;

namespace contractTesting.Test.Middleware
{
    class ProviderStateMiddleware
    {
        private const string ConsumerName = "CRM-UA";
        private readonly RequestDelegate _next;
        private readonly IDictionary<string, Action> _providerStates;
        private int i = 0;
        private dynamic contractFile;

        public ProviderStateMiddleware(RequestDelegate next)
        {
            _next = next;
            _providerStates = new Dictionary<string, Action>
            {
                { "Invoking GetUserByUserId", DummyMethod },
                { "Invoking GetAllUser", DummyMethod },
                { "Invoking AddUser", DummyMethod }

            };
        }
        private void DummyMethod()
        {
            //Blank method for provider state for no tasks
        }

        private void DeleteUser()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("https://my-tdd-application-api20210405124753.azurewebsites.net");
            var request = new HttpRequestMessage(HttpMethod.Get, "/api/user/1002");
            request.Headers.Add("Accept", "application/json");
            var response = _client.SendAsync(request);
            var content = JsonConvert.DeserializeObject<User>(response.Result.Content.ReadAsStringAsync().Result);
            var status = response.Result.StatusCode;
            if (content.userId == 1002)
            {
                var request1 = new HttpRequestMessage(HttpMethod.Delete, "/api/user/1002");
                request1.Headers.Add("Accept", "application/json");
                var response1 = _client.SendAsync(request1);
            }
        }

        public async Task Invoke(HttpContext context)
        {

            if (context.Request.Path.Value == "/provider-states")
            {
                this.HandleProviderStatesRequest(context);
                await context.Response.WriteAsync(String.Empty);

            }
            else
            {
                await this._next(context);
            }
        }

        private void HandleProviderStatesRequest(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            string jsonRequestBody = String.Empty;
            StreamReader reader = new StreamReader(@"../../../pacts/myconsumer-mytddapplication.json", Encoding.UTF8);
            jsonRequestBody = reader.ReadToEnd();

            contractFile = JValue.Parse(jsonRequestBody.ToString());
            while (i < contractFile.interactions.Count)
            {
                _providerStates[contractFile.interactions[i].providerState.Value].Invoke();
                i++;

            }

        }
    }
}