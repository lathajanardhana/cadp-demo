﻿using contractTesting.Consumer;
using contractTesting.Consumer.Mock;
using contractTesting.Utilities;
using my_tdd_application_api.Models;
using PactNet.Matchers;
using PactNet.Mocks.MockHttpService;
using PactNet.Mocks.MockHttpService.Models;
using System.Collections.Generic;

using Xunit;

namespace contractTesting.Test
{
    public class ConsumerPactTest :IClassFixture<ConsumerPact>
    {
        private IMockProviderService _mockProviderService;
        private string _mockProviderServiceBaseUri;

        public ConsumerPactTest(ConsumerPact data)
        {
            _mockProviderService = data.MockProviderService;
            _mockProviderService.ClearInteractions();
            _mockProviderServiceBaseUri = data.MockProviderServiceBaseUri;
        }

        [Fact]
        public void UserController_GetUserByUserId()
        {
            Course course = null;
            //Arrange
            _mockProviderService
                .Given("Invoking GetUserByUserId")
                .UponReceiving("A GET request to retrieve the Specific User details")
                .With(new ProviderServiceRequest
                {
                    Method = HttpVerb.Get,
                    Path = "/api/user/1001",
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" },
                        { "Server", "Kestrel"},

                    },
                    Body = new
                    {
                        userId = 1001,
                        userName = "myFirstUser",
                        description = "userDescription",
                        userRole = new
                        {
                            roleId = 101,
                            roleName = "myFirstRoleName"
                        },
                        course = course
                    }
                });

            //Act 
            var consumer = new UserClient(_mockProviderServiceBaseUri);
            var result = consumer.GetUserByUserId();
            Assert.Equal(1001, result.userId);
            _mockProviderService.VerifyInteractions();
        }

        [Fact]
        public void UserController_GetAllUser()
        {
            Course course = null;
            //Arrange
            _mockProviderService
                .Given("Invoking GetAllUser")
                .UponReceiving("A GET request to retrieve all Users")
                .With(new ProviderServiceRequest
                {

                    Method = HttpVerb.Get,
                    Path = "/api/user",
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" },
                        { "Server", "Kestrel"},

                    },
                    Body = Match.MinType(new
                    {

                        userId = 1001,
                        userName = "myFirstUser",
                        description = "userDescription",
                        userRole = new
                        {
                            roleId = 101,
                            roleName = "myFirstRoleName"
                        },
                        course = course

                    }, 1)
                });


            //Act 
            var consumer = new UserClient(_mockProviderServiceBaseUri);
            var result = consumer.GetAllUser();
            Assert.Equal(1001, result[0].userId);
            _mockProviderService.VerifyInteractions();
        }

        [Fact]
        public void UserController_AddUser()
        {
            Course course = null;
            //Arrange
            _mockProviderService
                .Given("Invoking AddUser")
                .UponReceiving("A POST request to add a User")
                .With(new ProviderServiceRequest
                {
                    Method = HttpVerb.Post,
                    Path = "/api/user",
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" }
                    },
                    Body = new
                    {
                        userId = Utility.userId,
                        userName = "SecondUser",
                        description = "myUserDescription",
                        userRole = new
                        {
                            roleId = 101,
                            roleName = "myFirstRoleName"
                        },
                        course = course
                    }
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "text/plain; charset=utf-8" },
                        { "Server", "Kestrel"},

                    },
                    Body = "User is added"
                });


            //Act
            User myUser = new User() { userId = Utility.userId, userName = "SecondUser", description = "myUserDescription", userRole = new Roles() { roleId = 101, roleName = "myFirstRoleName" } };
            var consumer = new UserClient(_mockProviderServiceBaseUri);
            var result = consumer.AddUser(myUser);
            Assert.Equal("User is added", result);
            _mockProviderService.VerifyInteractions();
        }
    }
}
