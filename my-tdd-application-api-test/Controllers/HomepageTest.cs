﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using my_tdd_application_api.Controllers;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using NSubstitute;
using System.Collections.Generic;

namespace my_tdd_application_api_test.Controllers
{
    [TestClass]
    public class HomepageTest
    {
        [TestMethod]
        public void myHomepage_returnsAllUserList()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<Homepage>>();
            var _userService = Substitute.For<IUserService>();
            var expected = new List<User>(){
            new User(){userId=1001,userName="myFirstUser",description="userDescription",userRole=new Roles(){roleId=101,roleName="myFirstRoleName"}}
            };
            _userService.returnAllUserList().Returns(expected);
            //Act
            Homepage homepage = new Homepage(_iLogger, _userService);
            var result = homepage.myHomepage();
            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}