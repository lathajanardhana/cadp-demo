﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using my_tdd_application_api.Controllers;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using NSubstitute;
using System.Collections.Generic;

namespace my_tdd_application_api_test.Controllers
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void GetAllUser_returnsAllUserList()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<UserController>>();
            var _userService = Substitute.For<IUserService>();
            var expected = new List<User>(){
            new User(){userId=1001,userName="myTestUser",description="userDescription",userRole=new Roles(){roleId=101,roleName="myFirstRoleName"}}};
            _userService.returnAllUserList().Returns(expected);
            //Act
            UserController userController = new UserController(_iLogger, _userService);
            var result = userController.GetAllUser();
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetUserByUserId_callsreturnUserDetailsforUserId()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<UserController>>();
            var _userService = Substitute.For<IUserService>();
            var expected = new User() { userId = 1001, userName = "myFirstUser", description = "userDescription", userRole = new Roles() { roleId = 101, roleName = "myFirstRoleName" } };
            _userService.returnUserDetailsforUserId(Arg.Any<int>()).Returns(expected);
            //Act
            UserController userController = new UserController(_iLogger, _userService);
            var result = userController.GetUserByUserId(1001);
            //Assert
            Assert.AreEqual(expected, result);
            _userService.Received(1).returnUserDetailsforUserId(Arg.Any<int>());
        }

        [TestMethod]
        public void AddUser_addsuserforvalidresponse()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<UserController>>();
            var _userService = Substitute.For<IUserService>();
            var returnData = true;
            _userService.AddUserToList(Arg.Any<User>()).Returns(returnData);
            //Act
            UserController userController = new UserController(_iLogger, _userService);
            var myUser = new User();
            var result = userController.AddUser(myUser);
            //Assert
            Assert.AreEqual("User is added", result);
        }

        [TestMethod]
        public void AddUser_returnserrorforinvalidresponse()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<UserController>>();
            var _userService = Substitute.For<IUserService>();
            var returnData = false;
            _userService.AddUserToList(Arg.Any<User>()).Returns(returnData);
            //Act
            UserController userController = new UserController(_iLogger, _userService);
            var myUser = new User();
            var result = userController.AddUser(myUser);
            //Assert
            Assert.AreEqual("Invalid User to add", result);
        }

        [TestMethod]
        public void UpdateUserRole_CallsupdateUserRoleANDreturnUserDetailsforUserId()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<UserController>>();
            var _userService = Substitute.For<IUserService>();           
            _userService.updateUserRole(Arg.Any<int>(), Arg.Any<int>()).Returns(true);
            _userService.returnUserDetailsforUserId(Arg.Any<int>()).Returns(new User());
            //Act
            UserController userController = new UserController(_iLogger, _userService);
            var result = userController.UpdateUserRole(1001,102);
            //Assert
            Assert.AreEqual(new User().GetType(), result.GetType());
            _userService.Received(1).updateUserRole(1001, 102);
            _userService.Received(1).returnUserDetailsforUserId(1001);
        }

        [TestMethod]
        [DataRow(1001)]
        public void RefreshUserCourse_callsupdateUserCourseandreturnuserdetails(int userId)
        {
            var _iLogger = Substitute.For<ILogger<UserController>>();
            var _userService = Substitute.For<IUserService>();
            var expected = new User() { };
            _userService.updateUserCourse(Arg.Any<int>()).Returns(true);
            _userService.returnUserDetailsforUserId(Arg.Any<int>()).Returns(expected);
            UserController userController = new UserController(_iLogger, _userService);
            var result = userController.RefreshUserCourse(userId);
            _userService.Received(1).updateUserCourse(Arg.Any<int>());
            Assert.AreEqual(expected, result);
        }

    }
}