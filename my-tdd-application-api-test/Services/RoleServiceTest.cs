﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using my_tdd_application_api.Models;
using my_tdd_application_api.Services;

namespace my_tdd_application_api_test.Services
{
    [TestClass]
    public class RoleServiceTest
    {
        [TestMethod]
        public void returnAllRoleList_returnsAllroleListfromsystem()
        {
            //Act
            RoleService roleService = new RoleService();
            var result = roleService.returnAllRoleList();
            //Assert
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void returnRoleDetailsforRoleId_returnsspecificRoledetails()
        {
            //Act
            RoleService roleService = new RoleService();
            var result = roleService.returnRoleDetailsforRoleId(102);
            //Assert
            Assert.AreEqual("myTestingRoleName", result.roleName);
        }

        [TestMethod]
        public void AddRoleToList_returnsfalseforinvalidRole()
        {
            //Act
            RoleService roleService = new RoleService();
            var myRole = new Roles() { roleId = 102, roleName = "myNewRoleName" };
            var result = roleService.AddRoleToList(myRole);
            //Assert
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void AddRoleToList_returnstrueforvalidRole()
        {           
            //Act
            RoleService roleService = new RoleService();
            var myRole = new Roles() { roleId = 103, roleName = "myNewRoleName" };
            Assert.AreEqual(false, roleService.returnAllRoleList().Contains(myRole));            
            var result = roleService.AddRoleToList(myRole);
            //Assert
            Assert.AreEqual(true, result);
            Assert.AreEqual(true, roleService.returnAllRoleList().Contains(myRole));
        }

    }
}