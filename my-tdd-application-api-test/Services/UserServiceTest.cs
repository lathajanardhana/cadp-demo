﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using my_tdd_application_api.Services;
using NSubstitute;
using System.Collections.Generic;
using System.Data;

namespace my_tdd_application_api_test.Services
{
    [TestClass]
    public class UserServiceTest
    {
        [TestMethod]
        public void returnAllUserList_returnsAllUserListfromsystem()
        {
            //Arrange
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            //Act
            UserService userService = new UserService(_roleService, _courseService);
            var result = userService.returnAllUserList();
            //Assert
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public void returnUserDetailsforUserId_returnsspecificUserdetails()
        {
            //Arrange
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            //Act
            UserService userService = new UserService(_roleService, _courseService);
            var result = userService.returnUserDetailsforUserId(1001);
            //Assert
            Assert.AreEqual("myFirstUser", result.userName);
        }

        [TestMethod]
        public void returnUserDetailsforUserId_returnserrorUserdetailforinvaliduserId()
        {
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            UserService userService = new UserService(_roleService, _courseService);
            var result = userService.returnUserDetailsforUserId(1002);
            Assert.AreEqual("InvalidUserId", result.userName);
            Assert.AreEqual(0000, result.userId);
        }

        [TestMethod]
        [DataRow(1001, "MyName", "MyDes")]
        [DataRow(100, "MyName", "MyDes")]
        [DataRow(1001, "", "MyDes")]
        [DataRow(1001, "MyName", "")]
        public void AddUserToList_returnsfalseforinvalidUser(int userId, string userName, string description)
        {
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            UserService userService = new UserService(_roleService, _courseService);
            var myUser = new User() { userId = userId, userName = userName, description = description };
            var result = userService.AddUserToList(myUser);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void AddUserToList_returnstrueforvalidUser()
        {
            //Arrange
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            //Act
            UserService userService = new UserService(_roleService, _courseService);
            var myUser = new User() { userId = 1002, userName = "myNewUserName", description= "myUserDescription", userRole= new Roles() { roleId=101, roleName = "myFirstRoleName" } };
            Assert.AreEqual(false, userService.returnAllUserList().Contains(myUser));
            var result = userService.AddUserToList(myUser);
            //Assert
            Assert.AreEqual(true, result);
            Assert.AreEqual(true, userService.returnAllUserList().Contains(myUser));
        }

        [TestMethod]
        [DataRow(1001, 102)]
        [DataRow(1001, 104)]
        public void updateUserRole_returnstrueifroleisupdated(int userId, int roleId)
        {
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            var RoleList = new List<Roles>() {
                new Roles(){roleId=101,roleName="myFirstRoleName"},
                new Roles() { roleId = 102, roleName = "myTestingRoleName" }
                };
            var RoleList2 = new List<Roles>() {
                new Roles(){roleId=101,roleName="myFirstRoleName"},
                new Roles() { roleId = 102, roleName = "myTestingRoleName" },
                new Roles() { roleId = roleId, roleName = roleId.ToString() }
                };
            _roleService.returnAllRoleList().Returns(x => RoleList, x => RoleList2);
            //var _courseService = Substitute.For<ICourseService>();
            UserService userService = new UserService(_roleService, _courseService);
            Assert.AreEqual("myFirstRoleName", userService.returnUserDetailsforUserId(userId).userRole.roleName);
            var result = userService.updateUserRole(userId, roleId);
            Assert.AreEqual(true, result);
            if (roleId == 102) { Assert.AreEqual("myTestingRoleName", userService.returnUserDetailsforUserId(userId).userRole.roleName); }
            else { Assert.AreEqual(roleId.ToString(), userService.returnUserDetailsforUserId(userId).userRole.roleName); }
        }

        [TestMethod]
        [DataRow(1001, 103)]
        [DataRow(1002, 102)]
        public void updateUserRole_returnsfalseifroleisnotupdated(int userId, int roleId)
        {
            //Arrange
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            var RoleList = new List<Roles>() {
            new Roles(){roleId=101,roleName="myFirstRoleName"},
            new Roles() { roleId = 102, roleName = "myTestingRoleName" }
            };
            _roleService.returnAllRoleList().Returns(RoleList);
            //Act
            UserService userService = new UserService(_roleService, _courseService);
            Assert.AreEqual("myFirstRoleName", userService.returnUserDetailsforUserId(1001).userRole.roleName);
            var result = userService.updateUserRole(userId, roleId);
            //Assert
            Assert.AreEqual(false, result);
            Assert.AreEqual("myFirstRoleName", userService.returnUserDetailsforUserId(1001).userRole.roleName);
        }

        [TestMethod]
        [DataRow(1002)]
        public void updateUserCourse_returnsfalseifusernotpresent(int userId)
        {
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            var CourseList = new List<Course>() {
            new Course(){cId=100, courseDescription="DefaultRole"},
            new Course() { cId=101, courseDescription="FirstRole" }
            };
            _courseService.returnAllCourses().Returns(CourseList);
            UserService userService = new UserService(_roleService, _courseService);
            var result = userService.updateUserCourse(userId);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        [DataRow(1001)]
        public void updateUserCourse_assignnewcreatedcourseifroleidnotincourselist(int userId)
        {
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            var CourseList = new List<Course>() {
            new Course(){cId=100, courseDescription="DefaultRole"}
            };
            var CourseList2 = new List<Course>() {
            new Course(){cId=100, courseDescription="DefaultRole"},
            new Course(){cId=101, courseDescription="101"}
            };
            _courseService.returnAllCourses().Returns(x => CourseList, x => CourseList2);
            UserService userService = new UserService(_roleService, _courseService);
            Assert.AreEqual(null, userService.returnUserDetailsforUserId(userId).course);
            var result = userService.updateUserCourse(userId);
            Assert.AreEqual(true, result);
            Assert.AreEqual("101", userService.returnUserDetailsforUserId(userId).course.courseDescription);
        }

        [TestMethod]
        [DataRow(1001)]
        public void updateUserCourse_assigncourseidsameasroleidforuser(int userId)
        {
            var _roleService = Substitute.For<IRoleService>();
            var _courseService = Substitute.For<ICourseService>();
            var CourseList = new List<Course>() {
            new Course(){cId=100, courseDescription="DefaultRole"},
            new Course() { cId=101, courseDescription="FirstRole" }
            };
            _courseService.returnAllCourses().Returns(CourseList);
            UserService userService = new UserService(_roleService, _courseService);
            Assert.AreEqual(null, userService.returnUserDetailsforUserId(userId).course);
            var result = userService.updateUserCourse(userId);
            Assert.AreEqual(true, result);
            Assert.AreEqual("FirstRole", userService.returnUserDetailsforUserId(userId).course.courseDescription);
        }

    }
}